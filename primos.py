
n = int(input("Dame un numero entero no negativo: "))
if n>0:
    print("Numeros primos iguales o menores: ")

    for i in range(1,n+1):       #establecemos la variable iteradora i, y la lista con range de parametros 1 (para que el programa
                                 # devuelva tambien) el 1 y n+1 (para que el programa tambien devuelva el numero ingresado).

        divisor = 2
        esPrimo = True           #variable booleana

        while esPrimo and divisor < i:              #bucle que solo se ejecuta si la variable booleana es cierta
            if i % divisor == 0:                    #si el resto del conciente entre i y divisor es 0, el numero no es primo, el bucle no se ejecutaria
                esPrimo = False
            else:                                   #si el resto no es 0, el numero ingresado es primo y el bucle se ejecuta.
                divisor += 1

        if esPrimo:
            print(i)

else:
    print("El numero que me has dado no es un entero negativo, TRY AGAIN :) :")








